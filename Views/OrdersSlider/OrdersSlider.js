import React, { Component } from 'react';
import Dimensions from 'Dimensions';
import Order from '../Order/Order';

import {
  View,
  FlatList,
  Text,
  Image,
  StyleSheet
} from 'react-native';

var moment = require('moment');
var { height, width } = Dimensions.get('window');

export default class OrdersSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowHeight: 0,
      data: [
        {
          date: new Date(),
          serviceType: 'Cleaning',
          avatar: require('./img/profile.png'),
          avatarType: 'filesystem',
          companyName: 'marble arch  repairs Ltd.',
          button: 'Click here for the bill ($190)'
        },
        {
          date: new Date(),
          serviceType: 'Cleaning',
          avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqRXztJ-Ia9EwzkOo9-QgrH4FBWJbVeanP9cyMteF2_1xMwIBu',
          avatarType: 'web',
          companyName: 'marble arch  repairs Ltd.',
          address: '1MW 2WA, London, UK'
        },
        {
          date: new Date(),
          serviceType: 'Cleaning',
          avatarType: 'filesystem',
          avatar: require('./img/profile.png'),
          companyName: 'marble arch  repairs Ltd.',
          button: 'Click here for the bill ($190)'
        }
      ]
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.data}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index}
          renderItem={({item, index}) => (
            <Order
              id={index}
              serviceType={item.serviceType}
              date={item.date}
              dateFormat='ddd, hh:mma'
              avatar={item.avatar}
              avatarType={item.avatarType}
              dataWithService='dataWithService'
              image='image'
              company='company'
              address={item.address}
              button={item.button}
              name={item.companyName}
              leftColor={this.props.leftColor}
              shadowColor={this.props.shadowColor}

            />
          )}
          horizontal={true}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 10,
    width: width,
    paddingHorizontal: 5,
  },
  shadow: {
    width: 4,
    height: 10,
    backgroundColor: 'red',
    position: 'absolute',
    top: 0,
    left: 0,
    shadowOffset: { width: -3, height: 0 },
    shadowOpacity: 0.4,
    shadowRadius: 4,
  },
  orderInfo: {
    width: 220,
    height: 75,
    margin: 5,
    backgroundColor: 'rgba(72,80,89, 0.5)',
  },
  clickForBill: {
    fontSize: 13,
    fontFamily: 'Montserrat-Light',
    color: 'white',
    textAlign: 'center',
    backgroundColor: 'transparent'
  },
  orderMonthAndName: {
    width: 90,
    height: 80,
    fontSize: 13,
    fontFamily: 'Montserrat-Light',
    color: 'white',
    textAlign: 'center',
    backgroundColor: 'transparent'
  },
});
