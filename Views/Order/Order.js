import React, { Component } from 'react';
import Dimensions from 'Dimensions';

import {
    View,
    FlatList,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

var moment = require('moment');
var { height, width } = Dimensions.get('window');

export default class Order extends Component {
    constructor(props){
      super(props)
      this.state = {
        shadowHeight: 0,
      }
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={styles.orderInfo} onLayout={(event) => {
                this.setState({ shadowHeight: event.nativeEvent.layout.height })
            }}>

                <View style={[styles.shadow, {
                    height: this.state.shadowHeight,
                    backgroundColor: this.props.leftColor,
                    shadowColor: this.props.shadowColor,
                }]}></View>

                <View style={{ flexDirection: 'row', height: 45 }}>
                    <Text style={styles.orderMonthAndName}>{moment(this.props.date).format(this.props.dateFormat)} {this.props.serviceType}</Text>
                    {this.props.avatarType == 'filesystem' && (
                      <View style={styles.imageWrap}>
                        <Image source={this.props.avatar} style={styles.picture} />
                      </View>
                    )}
                    {this.props.avatarType == 'web' && (
                      <View style={styles.imageWrap}>
                        <Image source={{uri: this.props.avatar}} style={styles.picture} />
                      </View>
                    )}
                    <Text style={styles.orderMonthAndName}>{this.props.name}</Text>
                </View>
                {this.props.button && (
                  <View style={{ height: 20 }}>
                      <Text style={styles.clickForBill}>{this.props.button}</Text>
                  </View>
                )}
                {!this.props.button && this.props.address && (
                  <View style={{ height: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                      <Image resizeMode="contain" source={require('./img/location.png')} style={styles.icon} />
                      <Text style={styles.clickForBill}>{this.props.address}</Text>
                  </View>
                )}
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    icon: {
      width: 15,
      marginRight: 5,
    },
    imageWrap: {
      width: 35,
      height: 35,
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 0.4,
      shadowRadius: 4,
      shadowColor: "#B00702"
    },
    picture: {
      width: 35,
      height: 35,
      borderRadius: 18,
    },
    container: {
        flexDirection: 'row',
        marginTop: 10,
        width: width,
        paddingHorizontal: 5,
    },
    shadow: {
        width: 4,
        height: 10,
        backgroundColor: 'red',
        position: 'absolute',
        top: 0,
        left: 0,
        shadowOffset: { width: -3, height: 0 },
        shadowOpacity: 0.4,
        shadowRadius: 4,
    },
    orderInfo: {
        width: 245,
        height: 75,
        margin: 5,
        backgroundColor: 'rgba(72,80,89, 0.5)',
    },
    clickForBill: {
        fontSize: 13,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        textAlign: 'center',
        backgroundColor: 'transparent'
    },
    orderMonthAndName: {
        width: 100,
        height: 80,
        fontSize: 13,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        textAlign: 'center',
        backgroundColor: 'transparent'
    },
});
