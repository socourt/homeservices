import React, { Component } from 'react';

import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet
} from 'react-native';

import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default class OrderCard extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return (
      <TouchableOpacity onPress={this.props.onPress}>
      <View style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        borderRadius: 5,
      }}>
        <View style={styles.imageContainer}>
          <Image source={require('./img/image.png')} style={styles.imageTop} />
          <View style={styles.locationUpperContainer}>
            <TouchableOpacity style={styles.locationContainer}>
              <Image source={require('./img/tabbar_icon_location.png')} style={{ width: 20, height: 20 }} />
              <Text style={{ lineHeight: 25, fontSize: 14, fontFamily: 'Montserrat-Light', color: 'white', backgroundColor: 'transparent' }}>9WA 0WS, London, UK</Text>
            </TouchableOpacity>
          </View>
        </View>

        {/* PLUMBING ISSUE */}
        <View style={[styles.problemDetailParts]}>
          <Text style={styles.problemTitleText}>Plumbing issue water waste</Text>
          <View style={styles.redLineContainer}>
            <View style={styles.redLine}></View>
          </View>

          <View style={styles.readMoreContainer}>
            <View style={styles.readMore}>
              <Image source={require('./img/eye.png')} style={{ width: 15, height: 9 }} />
              <Text style={styles.readMoreText}>Read More</Text>
            </View>
          </View>
          <View style={{ width: 60, height: 80, alignItems: 'center', flexDirection: 'column', position: 'absolute', right: 20, top: -20, }} >
              <Text style={{ backgroundColor: "transparent", color: 'black', fontSize: 9, fontFamily: 'Montserrat-Bold', textAlign: 'center' }}> CATEGORY </Text>
              <TouchableOpacity>
                  <View style={{ width: 45, height: 45, borderRadius: 23, borderColor: 'black', borderWidth: 2, alignItems: 'center' }}>
                      <Image source={require('./img/question-mark.png')} style={{ width: 30, height: 40, }} />
                  </View>
              </TouchableOpacity>
              <Text style={{ backgroundColor: "transparent", color: 'black', fontSize: 9, color: '#2b343e', fontFamily: 'Montserrat-Light', textAlign: 'center' }}>OTHERS</Text>
          </View>

        </View>
        <View style={[styles.problemDetailParts]}>

          <View style={[styles.devide, styles.borders]}>
            <Text style={styles.problemDescription}>There is a major issue in the bathroom - a huge water waste occurs</Text>
          </View>
          <View style={[styles.devide, {flexDirection: 'row', justifyContent: 'space-around', paddingHorizontal: 15}]}>

            <View style={[styles.fourColumns, {flexDirection: 'row'}]}>

              <Image source={require('./img/clock-bottom.png')} style={{ width: 20, height: 20 }} />
              <View style={{ width: 50, height: 20, borderRadius: 15, backgroundColor: '#ccd0db', flexDirection: 'row' }}>
                  <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: '#02124d', paddingLeft: 5, top: 3, backgroundColor: 'transparent' }}>5:00</Text>
                  <Text style={{ fontSize: 7, fontFamily: 'Montserrat-Light', color: '#02124d', top: 2, paddingLeft: 2, backgroundColor: 'transparent' }}>AM</Text>
              </View>

            </View>
            <View style={[styles.fourColumns, {width: (width-30)/4 - 20}]}>
              <View style={{ width: 35, height: 30, flexDirection: 'column', }}>
                  <Text style={{ fontSize: 15, fontFamily: 'Montserrat-Light', textAlign: 'center', color: '#02124d' }}>10</Text>
                  <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: '#6c7d8d' }}>THURS</Text>
              </View>
            </View>
            <View style={[styles.fourColumns, {width: (width-30)/4 - 20}]}>
                <Text style={{ fontSize: 13, fontFamily: 'Montserrat-Light', textAlign: 'center', color: '#02124d', }}>EUR149</Text>
            </View>
            <View style={[styles.fourColumns, {flexDirection: 'row'}]}>
              <Image source={require('./img/avatar.png')} style={{ width: 21, height: 21, }} />
              <Text style={{ fontSize: 9, fontFamily: 'Montserrat-Light', textAlign: 'center', color: '#02124d' }}>Marble Arch Repairs Ltd.</Text>
            </View>

          </View>

        </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  fourColumns: {
    width: (width-30)/4,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  borders: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopColor: '#dee0e1',
    borderBottomColor: '#dee0e1',
  },
  problemDescription: {
    backgroundColor: "transparent",
    fontSize: 13,
    width: width -60,
    textAlign: 'center',
    fontFamily: 'Montserrat-Light',
    color: '#2b343e'
  },
  devide: {
    height: ((height-175)/3)/4,
    width: width,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  category: {
    width: 50,
    height: 50,
    backgroundColor: 'red',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: -20,
    right: 20,
  },
  readMoreText: {
    fontSize: 9,
    backgroundColor: "transparent",
    fontFamily: 'Montserrat-Light',
    textAlign: 'center'
  },
  readMoreContainer: {
    width: width,
    height: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  readMore: {
    width: 70,
    height: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 2,
  },
  redLine: {
    width: 70,
    height: 2,
    backgroundColor: "#e47774",
    shadowColor: '#e47774',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  redLineContainer: {
    width: width,
    height: 5,
    display:'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  problemTitleText: {
    fontSize: 18,
    fontFamily: 'Montserrat-Light',
    textAlign: 'center',
    color: '#2b343e',
    backgroundColor: "transparent",
    width: (width/2) + 30,
  },
  problemDetailParts: {
    width: width,
    height: ((height-175)/3)/2,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  imageTop: {
    width: width - 30,
    height: (height-175)*2/3,

  },
  imageContainer: {
    width: width - 30,
    height: (height-175)*2/3,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    overflow: 'hidden',
  },
  topPart: {
    width: width - 30,
    height: 345,
    display: 'flex',
    alignItems: 'center',
    justifyContent:'center',
  },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#2f3842',
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width - 30,
        height: 345,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5
    },
    locationUpperContainer: {
      width: width,
      height: 25,
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'center',
      position: 'absolute',
      left: 0,
      bottom: 40,
    },
    locationContainer: {
        width: width*2/3,
        height: 25,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'rgba(44,57,66,0.5)',
        flexDirection: 'row',
        borderRadius: 5,
        paddingLeft: 5,
        // width: 200,
        // height: 35,
        // marginTop: 200,
        // borderRadius: 10,
        // justifyContent: 'center',
        // alignItems: 'center',
        // flexDirection: 'row',
        // backgroundColor: 'rgba(44,57,66,0.5)',
        // left: -60,
    },
    location: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
})
