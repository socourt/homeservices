import React, { Component } from 'react';
import {
    Platform,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';

var { height, width } = Dimensions.get('window');

export default class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
          more: false,
          icon: this.props.icon || 'menu',
          navigationType: this.props.navigationType || 'navigate',
          UpcomingColor: '#4b8d9c',
          FeaturedColor: '#1e575d',
          selected: 'Featured'
        }
    }

    componentWillMount(){
      // this.setState({more: this.props.moreOptions})
      console.log(this.state);
      if(this.props.selected && this.props.selected == 'Upcoming'){
        this.setState({
          UpcomingColor: '#4b8d9c',
          FeaturedColor: '#1e575d',
          selected: 'Upcoming'
        })
      }
      if(this.props.selected && this.props.selected == 'Featured'){
        this.setState({
          UpcomingColor: '#1e575d',
          FeaturedColor: '#4b8d9c',
          selected: 'Featured'
        })
      }
      // Upcoming
    }

    render() {

        return (
          <View style={{
            width: width,
            height: 50 + ((Platform.OS === 'ios') ? 20 : 0),
            display: 'flex',
          }}>
            <StatusBar
                barStyle="light-content"
                translucent={false}
            />
            <View style={ styles.statusBar }></View>


            <View style={styles.headerConainer}>

                {/* HEADER */}
                {this.state.icon == 'menu' && (
                  <TouchableOpacity style={styles.icon} onPress={this.props.pressMenu}>
                      <Image source={require('./img/icon_navbar_hamburger.png')} style={{ width: 20, height: 15 }} />
                  </TouchableOpacity>
                )}
                {this.state.icon == 'none' && (
                  <View style={styles.icon}></View>
                )}
                {this.state.icon == 'back' && (
                  <TouchableOpacity style={[styles.icon]} onPress={this.props.onPressBack}>
                      <Image resizeMode='contain' source={require('./img/back-icon.png')} style={{ width: 22 }} />
                  </TouchableOpacity>
                )}
                {this.state.icon == 'thumbs-up' && (
                  <TouchableOpacity style={styles.icon}>
                      <Image resizeMode='contain' source={require('./img/thumbsup.png')} style={{ width: 20 }} />
                  </TouchableOpacity>
                )}
                {this.state.icon  == 'notification' && (
                  <TouchableOpacity style={styles.icon} onPress={this.props.onPressNotification}>
                      <View style={{ width: 30, height: 30, borderRadius: 2, borderColor: 'white', borderWidth: 1, overflow: 'visible' }}>

                      </View>
                      <View style={{ position: 'absolute', top: 4, right: 4, width: 15, height: 15, borderRadius: 7, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center',borderColor:'white',borderWidth:1 }}>
                        <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Bold', color: '#0001ff', backgroundColor:'transparent'}}>8</Text>
                      </View>
                  </TouchableOpacity>
                )}
                <View style={styles.navigationContainer} >

                    {this.state.navigationType == 'navigate' && this.state.selected == 'Featured' && (
                      <TouchableOpacity onPress={this.props.onPressFeatured} underlayColor="rgba(253,138,94,0.2)" style={{
                          height: 30,
                          backgroundColor: this.state.UpcomingColor,
                          justifyContent: 'center',
                          alignItems: 'center',
                          flex: 2,
                          flexDirection: 'row',
                          borderRadius: 20,
                          zIndex: 2,
                      }}>
                          <View style={{
                            flex: 1,
                            height: 30,
                            justifyContent: 'center',
                            alignItems: 'center',
                            display: 'flex',
                            flexDirection: 'row',
                          }}>

                            <Image source={require('./img/featured-icon.png')} style={{ width: 30, height: 30 }} />
                            <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white' }}>FEATURED</Text>
                          </View>
                      </TouchableOpacity>
                    )}
                    {this.state.navigationType == 'navigate' && this.state.selected == 'Featured' && (
                      <TouchableOpacity onPress={this.props.onPressUpcoming} style={{
                          flex: 2,
                          height: 30,
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: this.state.FeaturedColor,
                          flexDirection: 'row',
                          borderTopRightRadius: 20,
                          borderBottomRightRadius: 20,
                          marginLeft: -20,
                          zIndex: 1,
                          paddingLeft: 10
                      }} >
                        <View style={{
                          width: 20,
                          height: 30,
                          borderTopRightRadius: 15,
                          borderBottomRightRadius: 15,
                          backgroundColor: '#3fabbe',
                          position: 'absolute',
                          top: 0,
                          left: 0,
                        }}></View>
                        <Image source={require('./img/Shape 119.png')} style={{ width: 30, height: 30 }} />
                        <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white' }}>UPCOMING</Text>
                      </TouchableOpacity>
                    )}

                    {this.state.navigationType == 'navigate' && this.state.selected == 'Upcoming' && (
                      <TouchableOpacity onPress={this.props.onPressFeatured} underlayColor="rgba(253,138,94,0.2)" style={{
                          height: 30,
                          backgroundColor: this.state.UpcomingColor,
                          justifyContent: 'center',
                          alignItems: 'center',
                          flex: 2,
                          flexDirection: 'row',
                          borderRadius: 20,
                          zIndex: 1,
                      }}>
                          <View style={{
                            flex: 1,
                            height: 30,
                            justifyContent: 'center',
                            alignItems: 'center',
                            display: 'flex',
                            flexDirection: 'row',
                          }}>

                            <Image source={require('./img/featured-icon.png')} style={{ width: 30, height: 30 }} />
                            <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white' }}>FEATURED</Text>
                          </View>
                      </TouchableOpacity>
                    )}
                    {this.state.navigationType == 'navigate' && this.state.selected == 'Upcoming' && (
                      <TouchableOpacity onPress={this.props.onPressUpcoming} style={{
                          flex: 2,
                          height: 30,
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: this.state.FeaturedColor,
                          flexDirection: 'row',
                          borderRadius: 20,
                          marginLeft: -20,
                          zIndex: 2,
                      }} >
                        <Image source={require('./img/Shape 119.png')} style={{ width: 30, height: 30 }} />
                        <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white' }}>UPCOMING</Text>
                      </TouchableOpacity>
                    )}

                    {this.state.navigationType == 'title' && (
                      <View style={[styles.titleContainer]}>
                        <Text style={[styles.titleText, this.props.titleStyles]}>{this.props.title.toUpperCase()}</Text>
                      </View>
                    )}
                </View>
                <View style={styles.spacer}>
                  {!this.props.close && !this.props.logout && this.props.moreOption && (
                    <TouchableOpacity>
                      <View style={{
                        width: 35,
                        height: 30,
                        borderRadius: 15,
                        backgroundColor: '#004ca0',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                        <View style={styles.dotContainer}>
                          <View style={styles.dot}></View>
                          <View style={styles.dot}></View>
                          <View style={styles.dot}></View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  )}
                  {!this.props.close && this.props.logout && (
                    <TouchableOpacity style={styles.icon}>
                        <Image resizeMode='contain' source={require('./img/logout.png')} style={{ width: 20 }} />
                    </TouchableOpacity>
                  )}
                  {this.props.close && (
                    <TouchableOpacity style={styles.icon} onPress={this.props.onClose}>
                        <Image resizeMode='contain' source={require('./img/close.png')} style={{ width: 20 }} />
                    </TouchableOpacity>
                  )}
                </View>
            </View>
          </View>
        );
    }
}

const styles = StyleSheet.create({
  statusBar: {
    height: (Platform.OS === 'ios') ? 20 : 0,
    width: width,
    backgroundColor: '#3fabbe'
  },
  dotContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  dot: {
    width: 3,
    height: 3,
    borderRadius: 2,
    backgroundColor: 'white',
    marginLeft: 1,
  },
  spacer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  navigationContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 5,
  },
  titleContainer: {
    flex: 2,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end'
  },
  titleText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 13,
    fontFamily: 'Montserrat-Light',
  },
  icon: {
    flex: 1,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerConainer: {
      width: width,
      height: 50,
      display: 'flex',
      flexDirection: 'row',
      backgroundColor: '#3fabbe',
      justifyContent: 'flex-start',
      alignItems: 'center',
  },
});
