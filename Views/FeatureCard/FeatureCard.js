import React, { Component } from 'react';

import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet
} from 'react-native';

import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default class FeatureCard extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return (
      <TouchableOpacity onPress={this.props.onPress}>
      <View style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        borderRadius: 5,
      }}>
      {/*BUTTONS */}

        <View style={styles.imageContainer}>

          <Image source={require('./img/image.png')} style={styles.imageTop}>
            <View style={{ width: width-20, height: 40, flexDirection: 'row', alignItems: 'center', justifyContent: 'center',}}>
                <TouchableOpacity style={{
                    width: (width/3) - 10,
                    height: 30,
                    borderRadius: 15,
                    backgroundColor: '#004ca0',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 5,
                    
                }}>
                    <Text style={{ fontSize: 10, color: 'white', fontFamily: 'Montserrat-Light' }}>WATER SUPPLY</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{
                    width: (width/3) - 10,
                    height: 30,
                    borderRadius: 15,
                    backgroundColor: '#004ca0',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 5
                }}>
                    <Text style={{ fontSize: 10, color: 'white', fontFamily: 'Montserrat-Light' }}>PLUMBING</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{
                    width: (width/3) - 10,
                    height: 30,
                    borderRadius: 15,
                    backgroundColor: '#004ca0',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <Text style={{ fontSize: 10, color: 'white', fontFamily: 'Montserrat-Light' }}>BATHTUBS</Text>
                </TouchableOpacity>
            </View>
          </Image>
          <View style={styles.locationUpperContainer}>
              <Text style={{ backgroundColor: 'transparent', fontSize: 25, color: 'white', fontFamily: 'Montserrat-Light' }}>Marble Arch Repairs Ltd.</Text>
              <View>
                  <Text style={{ backgroundColor: 'transparent',fontSize: 15, color: 'white', fontFamily: 'Montserrat-Light' }}>1MW 2WA, London, UK</Text>
                  <Image source={require('./img/current copy 3.png')} style={{ width: 72, height: 15, position: 'absolute', top: 10, left: -13 }} />
              </View>
          </View>
        </View>

        {/* PLUMBING ISSUE */}
        <View style={[styles.problemDetailParts]}>
          <View style={[styles.problemDetailPartsHalf, {height: ((height)/3)/4-10}]}>
            <View style={{ flexDirection: 'row', marginRight: 10, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity>
                    <Image source={require('./img/like_icon.png')} style={{ width: 20, height: 20, marginRight: 5 }} />
                </TouchableOpacity>
                <Text style={{ fontSize: 12, color: 'black' }}>45</Text>
            </View>
            <View style={{ flexDirection: 'row', marginRight: 10, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity>
                    <Image source={require('./img/comment_icon.png')} style={{ width: 20, height: 20, marginRight: 5 }} />
                </TouchableOpacity>
                <Text style={{ fontSize: 12, color: 'black', fontFamily: 'Montserrat-Light' }}>REVIEWS</Text>
            </View>
          </View>
          <View style={[styles.problemDetailPartsHalf, {height: ((height)/3)/4+10, alignItems: 'flex-start', paddingTop: 10}]}>
            <Text style={{ fontSize: 13, color: 'black', fontFamily: 'Montserrat-Light' }}>The Marble Arch Repairs Ltd. is a popular across London for its</Text>
          </View>
          <TouchableOpacity style={{ width: 70, height: 80,justifyContent: 'center', alignItems: 'center', flexDirection: 'row', position: 'absolute', right: 20, top: -60}}>
            <Image resizeMode="contain" source={require('./img/Ellipse 28 copy 2.png')} style={{ width: 70, display: 'flex', alignItems:'center', justifyContent: 'center'}}>
              <Text style={{ backgroundColor: 'transparent',fontSize: 13, color: 'black', fontFamily: 'Montserrat-Light'}}>ORDER</Text>
            </Image>
          </TouchableOpacity>

        </View>

        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  fourColumns: {
    width: (width-30)/4,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  borders: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopColor: '#dee0e1',
    borderBottomColor: '#dee0e1',
  },
  problemDescription: {
    backgroundColor: "transparent",
    fontSize: 13,
    width: width -60,
    textAlign: 'center',
    fontFamily: 'Montserrat-Light',
    color: '#2b343e'
  },
  devide: {
    height: ((height-195)/3)/4,
    width: width,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  category: {
    width: 50,
    height: 50,
    backgroundColor: 'red',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: -20,
    right: 20,
  },
  readMoreText: {
    fontSize: 9,
    backgroundColor: "transparent",
    fontFamily: 'Montserrat-Light',
    textAlign: 'center'
  },
  readMoreContainer: {
    width: width,
    height: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  readMore: {
    width: 70,
    height: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 2,
  },
  redLine: {
    width: 70,
    height: 2,
    backgroundColor: "#e47774",
    shadowColor: '#e47774',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  redLineContainer: {
    width: width,
    height: 5,
    display:'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  problemTitleText: {
    fontSize: 18,
    fontFamily: 'Montserrat-Light',
    textAlign: 'center',
    color: '#2b343e',
    backgroundColor: "transparent",
    width: (width/2) + 30,
  },
  problemDetailParts: {
    width: width,
    height: ((height)/3)/2,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  problemDetailPartsHalf: {
    width: width,
    height: ((height)/3)/4,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  imageTop: {
    width: width,
    height: (height-90)*2/3,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  imageContainer: {
    width: width,
    height: (height-90)*2/3,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    overflow: 'hidden',
    
  },
  topPart: {
    width: width - 30,
    height: 345,
    display: 'flex',
    alignItems: 'center',
    justifyContent:'center',
  },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#2f3842',
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width - 30,
        height: 345,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5
    },
    locationUpperContainer: {
      width: width,
      height: 25,
      paddingLeft: 10,
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'center',
      position: 'absolute',
      left: 0,
      bottom: 100,
    },
    locationContainer: {
        width: width*2/3,
        height: 25,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'rgba(44,57,66,0.5)',
        flexDirection: 'row',
        borderRadius: 5,
        paddingLeft: 5,
    },
    location: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
})
