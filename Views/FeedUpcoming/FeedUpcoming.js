import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {
  Platform,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  StatusBar,
  TextInput,
  ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';
import Swiper from 'react-native-swiper';

import OrderCard from '../OrderCard/OrderCard';

var { height, width } = Dimensions.get('window');

export default class FeedUpcoming extends Component {
  static navigationOptions = {
    title: 'FeedUpcoming',
  }
  constructor(props) {
    super(props)
  }
  render() {

    return (
      <View style={styles.container}>
        <Header
          icon="notification"
          selected="Upcoming"
          onPressFeatured={() => {
            this.props.navigation.navigate('FeedFeatured');
          }}
          onPressNotification={() => {
            this.props.navigation.navigate('NotificationScr');
          }}
        />
        {/* MAIN CONTAINER */}
        <View style={{
          width: width - 30,
          height: height - 150,
          marginBottom: 20,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-start',
          backgroundColor: 'transparent',
          marginVertical: 12,
          borderRadius: 5
        }}>
          <Swiper
            style={styles.wrapper}
            showsButtons={false}
            containerStyle={{ width: Dimensions.get("window").width, borderRadius: 5, paddingBottom: 20 }}
            dot={<View style={{ backgroundColor: 'white', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
            activeDot={<View style={{ backgroundColor: 'white', width: 12, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
            paginationStyle={{
              position: 'absolute',
              bottom: 0,
            }}
          >
            <View style={styles.slide}>
              <OrderCard onPress={() => {
                this.props.navigation.navigate('Summary', { type: "User" });
              }} />
            </View>
            <View style={styles.slide}>
              <OrderCard onPress={() => {
                this.props.navigation.navigate('Summary', { type: "User" });
              }} />
            </View>
            <View style={styles.slide}>
              <OrderCard onPress={() => {
                this.props.navigation.navigate('Summary', { type: "User" });
              }} />
            </View>
          </Swiper>
        </View>
        {/* FOOTER */}
        <Footer
          icon={['user', 'home']}
          hightlightedIcon="home"
          navigationType="navigate"
          userPress={() => {
            this.props.navigation.navigate('MyProfile');
          }}
          bellPress={() => {
            this.props.navigation.navigate('Home');
          }}
        />
      </View >

    );
  }
}
// #e47774
const styles = StyleSheet.create({

  wrapper: {
    borderRadius: 5,
    overflow: 'visible'
  },
  slide: {
    backgroundColor: 'white'
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  fourColumns: {
    width: (width - 30) / 4,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  borders: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopColor: '#dee0e1',
    borderBottomColor: '#dee0e1',
  },
  problemDescription: {
    backgroundColor: "transparent",
    fontSize: 13,
    width: width - 60,
    textAlign: 'center',
    fontFamily: 'Montserrat-Light',
    color: '#2b343e'
  },
  devide: {
    height: ((height - 145) / 3) / 4,
    width: width,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  category: {
    width: 50,
    height: 50,
    backgroundColor: 'red',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: -20,
    right: 20,
  },
  readMoreText: {
    fontSize: 9,
    backgroundColor: "transparent",
    fontFamily: 'Montserrat-Light',
    textAlign: 'center'
  },
  readMoreContainer: {
    width: width,
    height: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  readMore: {
    width: 70,
    height: 30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 2,
  },
  redLine: {
    width: 70,
    height: 2,
    backgroundColor: "#e47774",
    shadowColor: '#e47774',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  redLineContainer: {
    width: width,
    height: 5,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  problemTitleText: {
    fontSize: 18,
    fontFamily: 'Montserrat-Light',
    textAlign: 'center',
    color: '#2b343e',
    backgroundColor: "transparent",
    width: (width / 2) + 30,
  },
  problemDetailParts: {
    width: width,
    height: ((height - 145) / 3) / 2,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  imageTop: {
    width: width - 30,
    height: (height - 145) * 2 / 3,

  },
  imageContainer: {
    width: width - 30,
    height: (height - 145) * 2 / 3,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    overflow: 'hidden',
  },
  topPart: {
    width: width - 30,
    height: 345,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#2f3842',
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: width - 30,
    height: 345,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5
  },
  locationUpperContainer: {
    width: width,
    height: 25,
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    position: 'absolute',
    left: 0,
    bottom: 40,
  },
  locationContainer: {
    width: width * 2 / 3,
    height: 25,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(44,57,66,0.5)',
    flexDirection: 'row',
    borderRadius: 5,
    paddingLeft: 5,
    // width: 200,
    // height: 35,
    // marginTop: 200,
    // borderRadius: 10,
    // justifyContent: 'center',
    // alignItems: 'center',
    // flexDirection: 'row',
    // backgroundColor: 'rgba(44,57,66,0.5)',
    // left: -60,
  },
  location: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },


});
