import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,

} from 'react-native';
import Dimensions from 'Dimensions';
import Swipeout from 'react-native-swipeout';


var moment = require('moment');
// import * as moment from 'moment';


var { height, width } = Dimensions.get('window');

export default class Notification extends Component {

    constructor(props) {
        super(props);
        this.state = {
          shadowHeight: 0,
        }
    }

    parseButtons(type){
      let buttons = [];
      if(type == 'view'){
        buttons.push(
          {
            component: (
              <View style={styles.buttonContainer}>
                <Image source={require('./img/glasses.png')} style={{width: 27, height: 20}} />
                <Text style={{color: 'white', backgroundColor: 'transparent'}}>view</Text>
              </View>
            ),
            backgroundColor: 'transparent'
          }
        )
      }else if(type == 'reply'){
        buttons.push(
          {
            component: (
              <View style={styles.buttonContainer}>
                <Image source={require('./img/conversation-.png')} style={{width: 23, height: 20}} />
                <Text style={{color: 'white', backgroundColor: 'transparent'}}>reply</Text>
              </View>
            ),
            backgroundColor: 'transparent'
          }
        )
      }
      buttons.push({
        component: (
          <View style={styles.buttonContainer}>
            <Image source={require('./img/cancel.png')} style={{width: 20, height: 20}} />
            <Text style={{color: 'white', backgroundColor: 'transparent'}}>clear</Text>
          </View>
        ),
        backgroundColor: 'transparent'
      });

      return buttons;
    }

    render() {
        return (
            <Swipeout buttonWidth={80} autoClose={true} right={this.parseButtons(this.props.type)} style={[styles.swipe]}>
              <View style={styles.notification} onLayout={(event) => {
                this.setState({shadowHeight: event.nativeEvent.layout.height})
              }}>
                <View style={[styles.shadow, {
                  height: this.state.shadowHeight,
                  backgroundColor: this.props.color,
                  shadowColor: this.props.color,
                }]}></View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('./img/clock-bottom.png')} style={{ width: 25, height: 25 }} />
                    <Text style={styles.time}>{ moment(this.props.datetime).format('HH:mm DD MMM YYYY') }</Text>
                </View>
                <Text style={styles.message}> {this.props.message}</Text>
              </View>
            </Swipeout>
        );
    }
}




const styles = StyleSheet.create({
  buttonContainer: {
    flex: 1,
    alignItems:'center',
    justifyContent: 'center',
  },
  shadow: {
    width: 4,
    height: 10,
    backgroundColor: 'red',
    position: 'absolute',
    top: 0,
    left: 0,
    shadowOffset: { width: -1, height: 0 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  swipe: {
    width: width - 30,
    backgroundColor: 'transparent',
    marginTop: 10,
    overflow: 'visible'
  },
  notification: {
    width: width - 30,
    backgroundColor: 'rgba(71,71,74, 0.5)',
    paddingVertical: 10,
    paddingLeft: 10,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  time: {
    fontSize: 12,
    fontFamily: 'Montserrat-Light',
    color: '#cececf'
  },
  message: {
    marginTop: 2,
    fontSize: 14,
    fontFamily: 'Montserrat-Light',
    color: '#fffffe'
  },
});
