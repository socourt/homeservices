import React, { Component } from 'react';
import {
    Platform,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';


var { height, width } = Dimensions.get('window');

export default class SingInFormPopup extends Component {
    static navigationOptions = {
        title: 'SingInFormPopup',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>

                <Image source={require('./img/background.png')} style={styles.backgraundImg} />

                {/* SING IN */}
                <View style={styles.singinContainer}>
                    <View style={styles.popupContainer}>


                        {/* HEADER */}
                        <View style={styles.headerContainer} >
                            <Text style={[styles.textStyle, { fontSize: 18, color: 'black' }]}>SIGN IN </Text>
                            <TouchableOpacity style={styles.cancelButton}>
                                <Image source={require('./img/cancel.png')} style={{ width: 20, height: 20 }} />
                            </TouchableOpacity>
                        </View>

                        {/* TEXT INPUT */}

                        <View style={styles.textInputContainer} >
                            <View style={styles.textInputSection}>
                                <View style={{ flex: 1, }}>
                                    <Image source={require('./img/Rectangle 2 copy.png')} style={{ width: 25, height: 20, }} />
                                </View>
                                <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    placeholder="E-mail address "
                                    placeholderTextColor="#7f7f7f"
                                    style={styles.textInput}></TextInput>
                            </View>
                            <View style={styles.textInputSection}>
                                <View style={{ flex: 1, }}>
                                    <Image source={require('./img/lock icon.png')} style={{ width: 24, height: 28, }} />
                                </View>
                                <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    placeholder="............."
                                    placeholderTextColor="#7f7f7f"
                                    style={styles.textInput}></TextInput>
                            </View>
                        </View>




                        {/* BUTTON */}
                        <View style={{ flex: 2 }}>
                            <TouchableOpacity style={styles.button} onPress={() => {
                              this.props.navigation.navigate('FeedUpcoming');
                            }}>
                                <Text style={[styles.textStyle, { fontSize: 17, color: 'white'}]}>Sign in </Text>
                                <View style={[styles.cancelButton, { right: 5 }]}>
                                    <Image source={require('./img/Arrow.png')} style={{ width: 10, height: 19 }} />
                                </View>
                            </TouchableOpacity>

                            {/* FORGOT PASSWORD */}
                            <View style={{ marginTop: 10 }}>
                                <TouchableOpacity>
                                    <Text style={[styles.textStyle, { fontSize: 14, color: '#666666' }]}>Forgot password? </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View >
                </View >

                {/* SING IN WITH FACEBOOK*/}
                <View style={styles.singinFBContainer}>
                    <TouchableOpacity style={styles.fbButton}>
                        <Image source={require('./img/fb icon.png')} style={{ width: 6, height: 15, marginRight: 10, marginLeft: 10 }} />
                        <Text style={[styles.textStyle, { fontSize: 13, color: 'white'}]}>Sign in with Facebook </Text>
                    </TouchableOpacity>

                    <Text style={[styles.textStyle, { fontFamily: 'Montserrat-Light', fontSize: 13, color: '#1c3e44', }]}>Don’t have an account? </Text>


                    <TouchableOpacity>
                        <Text style={[styles.textStyle, { fontSize: 18, color: 'white'}]}>SIGN UP</Text>
                    </TouchableOpacity>

                </View >
            </View >


        );
    }
}

const styles = StyleSheet.create({
    singinFBContainer: {
        width: width,
        height: 150,
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
    },

    fbButton: {
        height: 30,
        width: width - 140,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3b5998'
    },
    button: {
        height: 40,
        width: width - 100,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#171e4b'
    },
    textInput: {
        fontSize: 14,
        fontFamily: 'Montserrat-Regular',
        color: 'white',
        flex: 4,
        marginRight: 10,
        fontSize: 18
    },
    textInputSection: {
        width: width - 100,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderBottomColor: '#d9d9d9',
        borderBottomWidth: 1,
        marginVertical: 10
    },
    textInputContainer: {
        flex: 6,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cancelButton: {
        position: 'absolute',
        right: 20,
        width: 20,
        height: 20
    },
    headerContainer: {
        width: width - 60,
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#dee0e1',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        height: 60,
        flex: 1
    },
    textStyle: {
        fontSize: 11,
        fontFamily: 'Montserrat-Regular',
        textAlign: 'center',
        backgroundColor: 'transparent'
    },
    popupContainer: {
        width: width - 60,
        height: (height * 5 / 6) - 100,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 15
    },

    singinContainer: {
        width: width,
        height: height - 150,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    container: {
        width: width,
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',

    },
    backgraundImg: {
        position: 'absolute',
        width: width,
        height: height,
        top: 0,
        left: 0,

    },
});
