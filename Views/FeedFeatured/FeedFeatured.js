import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {
    Platform,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';

import Swiper from 'react-native-swiper';


import Dimensions from 'Dimensions';
import FeatureCard from '../FeatureCard/FeatureCard';


var { height, width } = Dimensions.get('window');

export default class FeedFeatured extends Component {
    static navigationOptions = {
        title: 'FeedFeatured',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
          <View style={styles.container}>
            <Header
              icon="notification"
              selected="Featured"
              onPressUpcoming={() => {
                this.props.navigation.navigate('FeedUpcoming');
              }}
              onPressNotification={() => {
                this.props.navigation.navigate('NotificationScr');
              }}
            />
            {/* CONTAINER */}
            <View style={{ width: width , height: height - 135, backgroundColor: 'white', alignItems: 'center', marginTop: 20,borderColor:'black', borderWidth:1  }}>

              <Swiper
                style={styles.wrapper}
                showsButtons={false}
                containerStyle={{ width: Dimensions.get("window").width, borderRadius: 5, }}
                paginationStyle={{
                  position: 'absolute',
                  top: -20,
                  height: 20,
                }}
                dot={<View style={{backgroundColor: 'white', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, borderWidth: StyleSheet.hairlineWidth, borderColor: "#004ca0"}} />}
                activeDot={<View style={{backgroundColor: '#004ca0', width: 12, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
              >

                <View style={styles.slide}>
                  <FeatureCard />
                </View>
                <View style={styles.slide}>
                  <FeatureCard />
                </View>
                <View style={styles.slide}>
                  <FeatureCard />
                </View>
              </Swiper>

            </View>
            {/* FOOTER */}
            <Footer icon={['user', 'home']} hightlightedIcon="home"  navigationType="navigate"
             userPress={() => {
               this.props.navigation.navigate('MyProfile');
             }}
             bellPress={() => {
               this.props.navigation.navigate('Home');
             }}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
      overflow: 'visible'
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    headerConainer: {
        width: width,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 5
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: 370
    },
    footerContainer: {
        width: width,
        height: 45,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    }
});
