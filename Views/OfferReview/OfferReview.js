import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';
import Swiper from 'react-native-swiper';
import OffersSwiper from '../OffersSwiper/OffersSwiper';

var { height, width } = Dimensions.get('window');

export default class OfferReview extends Component {
    static navigationOptions = {
        title: 'OfferReview',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>
                {/* HEADER */}
                <Header icon="back" navigationType="navigate"
                onPressBack={() => {
                  this.props.navigation.goBack();
                }}
                onPressFeatured={() => {
                  this.props.navigation.navigate('FeedFeatured');
                }}
                onPressUpcoming={() => {
                  this.props.navigation.navigate('FeedUpcoming');
                }}
                />



                {/* MAIN CONTAINER */}
                <View style={{ width: width, height: 480, backgroundColor: 'white', alignItems: 'center', flex: 1, marginTop: 5 }}>

                    <View style={{ justifyContent: 'center', alignItems: 'center', height: 80, width: width - 20, flexDirection: 'row' }}>

                        {/* IMAGE CONTAINER */}
                        <View style={{ width: 60, height: 80, }}>
                            <View style={{ height: 80, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('./img/profile.png')} style={{ width: 50, height: 50, }} />
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center' }}>120 km away</Text>
                            </View>
                        </View>


                        {/* TEXT CONTAINER */}
                        <View style={{ width: width - 80, height: 80, justifyContent: 'flex-end' }}>
                            <View>
                                <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'black' }}>Monika Kim (sent: 19 Oct 2017 17:10) </Text>
                            </View>

                            <View style={{ backgroundColor: '#9d9db4', borderRadius: 10, height: 60 }}>
                                <View style={{ height: 20, flexDirection: 'row', }}>
                                    <TouchableOpacity style={{ width: 20, height: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('./img/tabbar_icon_location.png')} style={{ width: 50, height: 50, }} />
                                    </TouchableOpacity>
                                    <Text style={{ fontSize: 13, fontFamily: 'Montserrat-Light', color: 'white', textAlign: 'right', }}>1MW 2WA, London, UK ($190)</Text>
                                </View>
                                <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white', }}>Plumbing issue causing severe water waste.Lorem ispum dolor sit amet,adipiscing e </Text>
                            </View>
                        </View>
                    </View>


                    {/* IMG CONTAINER */}
                    <View style={{ marginTop: 5, display: 'flex', alignItems: 'center', justifyContent: 'center', width: width - 40, height: 160, overflow: 'hidden', borderRadius: 10}}>
                        <Image resizeMode={'contain'} source={require('./img/plumbing-issue.png')} style={{ width: width - 40}} />
                    </View>

                    <View style={{ marginVertical: 5, }}>
                        <Image source={require('./img/current copy 2.png')} style={{ width: width - 40, height: 4, }} />
                    </View>



                    {/* PLACED OFFERS */}


                    {/* BUTTONS */}

                    <View style={{
                      width: width,
                      height: height - 414,
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                      <Swiper
                        style={styles.wrapper}
                        showsButtons={true}
                        paginationStyle={{
                          bottom: -10,
                        }}
                        nextButton={(<View><Image source={require('./img/slider-arrow-left.png')} style={{ width: 15, height: 15, }} /></View>)}
                        prevButton={(<View><Image source={require('./img/slider-arrow.png')} style={{ width: 15, height: 15, }} /></View>)}
                        buttonWrapperStyle={{
                          backgroundColor: 'transparent',
                          flexDirection: 'row',
                          position: 'absolute',
                          top: 20,
                          left: 0,
                          flex: 1,
                          height: 30,
                          paddingHorizontal: 10,
                          paddingVertical: 10,
                          justifyContent: 'space-between',
                          alignItems: 'center'
                        }}
                        dot={<View style={{backgroundColor: 'white', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3, borderWidth: StyleSheet.hairlineWidth, borderColor: "#004ca0"}} />}
                        activeDot={<View style={{backgroundColor: '#004ca0', width: 12, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
                      >
                        <OffersSwiper />
                        <OffersSwiper />
                        <OffersSwiper />
                        <OffersSwiper />
                        <OffersSwiper />
                        <OffersSwiper />
                      </Swiper>

                    </View>

                </View>

                {/* FOOTER */}
                <Footer icon={['user', 'home']} hightlightedIcon="home" navigationType="navigate"
                userPress={() => {
                  this.props.navigation.navigate('MyProfile');
                }}
                homePress={() => {
                  this.props.navigation.navigate('FeedUpcoming');
                }}
                bellPress={() => {
                  // this.props.navigation.navigate('BusinessOrderReview');
                }}
                />
            </View >

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    headerConainer: {
        width: width,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: 470
    },
    locationContainer: {
        width: 200,
        height: 35,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(44,57,66,0.5)',
        left: -60
    },
    location: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    fourSectionsSmallContainer: {
        width: (width - 20) / 4 - 20,
        height: 50,
        flexDirection: 'row',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fourSectionsBigContainer: {
        width: (width - 20) / 4 + 20,
        height: 50,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fourSections: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: 50,
        flexDirection: 'row',
        marginTop: 5,
    },
    line: {
        width: width - 40,
        borderBottomColor: '#999ea3',
        borderBottomWidth: 1,
    },
});
