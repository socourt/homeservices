import React, { Component } from 'react';
import {
    Platform,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';


var { height, width } = Dimensions.get('window');

export default class InitialScreen extends Component {
    static navigationOptions = {
        title: 'InitialScreen',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>

                <StatusBar
                    backgroundColor="#3fabbe"
                    barStyle="light-content"
                />
                {/* BACKGROUND IMAGE */}
                <Image source={require('./img/backgroundImage.png')} style={styles.backgraundImg} />


                {/* WELCOME */}
                <View style={styles.topTextContainer}>
                    <View style={styles.topInnerContainer} >
                        <View style={{ width: width - 100 }} >
                            <Text style={[styles.textStyle, { fontSize: 25, lineHeight: 40 }]}>Welcome to  OrdenCo</Text>
                        </View>

                        <View>
                            <Image source={require('./img/bell.png')} style={{ width: 80, height: 80, }} />
                        </View>
                        <View style={{ width: width - 40 }}>
                            <Text style={[styles.textStyle, { fontSize: 20 }]}>Get the hottest providers for your home service repairs</Text>
                        </View>
                    </View>
                </View>

                {/* LOGIN CONTAINER */}
                <View style={styles.buttonContainer}>
                    <View style={styles.buttonInnerContainer} >
                        <TouchableOpacity onPress={() => {
                          this.props.navigation.navigate('SingInFormPopup');
                        }}>
                            <Text style={[styles.textStyle, { fontSize: 20, color: 'black' }]}>Sign in </Text>
                        </TouchableOpacity>

                        <View style={styles.verticalLine}>
                            <View style={styles.blacklLine}></View>
                        </View>

                        <TouchableOpacity>
                            <Text style={[styles.textStyle, { fontSize: 20, color: 'black' }]}>Tour </Text>
                        </TouchableOpacity>
                    </View >
                </View >




            </View >

        );
    }
}

const styles = StyleSheet.create({
    blacklLine: {
        width: 2,
        height: (height * 1 / 5) - 70,
        backgroundColor: 'black',

    },
    verticalLine: {
        width: 2,
        height: (height * 1 / 5) - 70,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonInnerContainer: {
        justifyContent: 'flex-start',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
        width: width,
        height: (height * 1 / 5) - 40,
        // backgroundColor: 'yellow'
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: width,
        height: height * 1 / 6,
         //backgroundColor: 'red'
    },
    textStyle: {
        fontSize: 11,
        fontFamily: 'Montserrat-Regular',
        color: 'white',
        textAlign: 'center',
        backgroundColor: 'transparent'
    },
    topInnerContainer: {
        display: 'flex',
        bottom:30,
        width: width,
        height: (height * 4 / 5) - 150,
        justifyContent: 'center',
        justifyContent: 'space-around',
        alignItems: 'center',
        //backgroundColor: 'pink',

    },
    topTextContainer: {
        width: width,
        height: height * 5 / 6,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        //backgroundColor: 'green'

    },
    backgraundImg: {
        position: 'absolute',
        width: width,
        height: height,
        top: 0,
        left: 0
    },
    container: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }

});
