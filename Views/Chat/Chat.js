import React, { Component } from 'react';
import Header from '../Header/Header';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';

var { height, width } = Dimensions.get('window');

export default class Summary extends Component {
    static navigationOptions = {
        title: 'Summary',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>
                <Image source={require('./img/company-team.png')} style={{ position: 'absolute', width: width, height: height, top: 0, left: 0 }} />
                {/* HEADER */}
                <Header icon="back" onPressBack={() => {
                  this.props.navigation.goBack();
                }} navigationType="title" title="CHAT MARBLE ARCH REPAIRS"/>
                {/* MAIN CONTAINER */}
                <View style={{ width: width, height: height-120, alignItems: 'center' }}>



                    <View style={{ marginTop: 110, width: width, height: 80, flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ marginLeft: 10, width: 160, height: 55, borderTopLeftRadius: 30, borderTopRightRadius: 30, borderBottomRightRadius: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center' }}>Lorem ipsum dolor sit amet adipiscing elit!</Text>
                        </View>
                    </View>

                    <View style={{ width: width, flexDirection: 'row', justifyContent: 'flex-end', marginTop: 10 }}>
                        <View style={{ flexDirection: 'column' }}>
                            <View style={{ width: width, marginRight: 10, width: 160, height: 30, borderTopLeftRadius: 30, borderTopRightRadius: 30, borderBottomLeftRadius: 30, backgroundColor: '#2962ff', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'white' }}>Lorem ipsum dolor?</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', marginRight: 10 }}>
                                <Image source={require('./img/green.png')} style={{ width: 15, height: 15, marginRight: 5 }} />
                                <Text style={{ backgroundColor: 'transparent', fontSize: 14, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'white' }}>10sec</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{ width: width, height: 80, flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ marginLeft: 10, width: 160, height: 55, borderTopLeftRadius: 30, borderTopRightRadius: 30, borderBottomRightRadius: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center' }}>Lorem ipsum dolor sit amet adipiscing elit!</Text>
                        </View>
                    </View>

                    <View style={{ width: width, flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <View style={{ marginRight: 10, width: 50, height: 30, borderTopLeftRadius: 30, borderTopRightRadius: 30, borderBottomLeftRadius: 30, backgroundColor: '#2962ff', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'white' }}>...</Text>
                        </View>
                    </View>


                </View>


                {/* FOOTER */}
                <View style={styles.footerContainer} >

                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity>
                            <Image source={require('./img/2.png')} style={{ width: 30, height: 30, marginRight: 15 }} />
                        </TouchableOpacity>
                        <TextInput
                            autoCorrect={false}
                            underlineColorAndroid="transparent"
                            placeholder="Type your message "
                            placeholderTextColor="#424242"
                            style={{ fontSize: 15, fontFamily: 'Montserrat-Light', textAlign: 'left', width: 200, color: 'white' }}></TextInput>
                    </View>

                    <TouchableOpacity style={{ width: 60, height: 35, borderRadius: 20, backgroundColor: '#2d60ac', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white', textAlign: 'center' }}>Send</Text>
                    </TouchableOpacity>

                </View>
              </View>

            );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'center',
      backgroundColor: 'white',
    },
    headerConainer: {
        width: width,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    absolute: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: width,
      height: 470
    },
    locationContainer: {
        width: 200,
        height: 35,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(44,57,66,0.5)',
        left: -60
    },
    location: {
                    width: width,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    footerContainer: {
        width: width,
        height: 55,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'black'
    }

});
