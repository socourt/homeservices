import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,
    KeyboardAvoidingView
} from 'react-native';
import Dimensions from 'Dimensions';

var { height, width } = Dimensions.get('window');

export default class BusinessAcceptDetails extends Component {
    static navigationOptions = {
        title: 'BusinessAcceptDetails',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>

                <View style={styles.popupContainer}>

                    <Image source={require('./img/Layer 2.png')} style={{ position: 'absolute', width: width, height: height, top: 0, left: 0 }} />

                    {/* HEADER */}
                    <View style={styles.header}>
                        <Text style={{ backgroundColor: 'transparent', fontSize: 18, fontFamily: 'Montserrat-Regular', color: 'white', textAlign: 'center' }}>ADD DETAILS TO ACCEPT</Text>
                        <TouchableOpacity onPress={this.props.onClose} style={{ alignItems: 'center', position: 'absolute', right: -35, height: 20 }}>
                            <Image source={require('./img/remove copy.png')} style={{ width: 20, height: 20, }} />
                        </TouchableOpacity>
                    </View>


                    {/* ADDRESS LOCATION */}
                    <View style={styles.addressLocation}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                                <Text style={{ backgroundColor: 'transparent', fontSize: 14, fontFamily: 'Montserrat-Regular', color: '#627485' }}>Location for the home service order:</Text>
                                <Text style={{ backgroundColor: 'transparent', fontSize: 14, fontFamily: 'Montserrat-Regular', color: '#58a4b0' }}>Ludwigstrasse 41, 80012, Munich</Text>
                            </View>
                        </View>
                    </View>


                    {/* IMAGES*/}
                    <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                        <Image source={require('./img/dash.png')} style={{ width: 90, height: 30, }} />
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                        <Text style={{ backgroundColor: 'transparent', fontSize: 13, fontFamily: 'Montserrat-Regular', color: '#e5eafa', }}>YOUR PRICE FOR THE SERVICE ORDER:</Text>
                        <TextInput
                            autoCorrect={false}
                            underlineColorAndroid="white"
                            placeholder="Add your price here "
                            placeholderTextColor="#e5eafa"
                            style={{ width: 130, fontSize: 12, fontFamily: 'Montserrat-Regular', color: 'white', }}></TextInput>

                        {/* LINE */}
                        <View
                            style={{
                                width: width - 80,
                                borderBottomColor: '#4a545d',
                                borderBottomWidth: 1,
                            }}
                        />
                        <Text style={{ backgroundColor: 'transparent', fontSize: 13, fontFamily: 'Montserrat-Regular', color: '#e5eafa', marginTop: 10 }}>PICK A DATE & TIME FOR REACTION</Text>

                    </View>



                    {/*DROPDOWN CONTAINER*/}
                    <View style={styles.brushContainer}>
                        <View style={{ width: 120, height: 30, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <Text style={{ color: '#e5eafa', fontFamily: 'Montserrat-Light' }}> 01.01.2017 </Text>
                            <Image source={require('./img/sort-down.png')} style={styles.sortdown} />
                        </View>

                        <View style={{ width: 120, height: 30, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <Text style={{ backgroundColor: 'transparent', color: '#e5eafa', fontFamily: 'Montserrat-Light' }}> 00:00 H </Text>
                            <Image source={require('./img/sort-down.png')} style={styles.sortdown} />
                        </View>
                    </View>

                    {/* LINE */}
                    <View
                        style={{
                            width: width - 80,
                            borderBottomColor: '#4a545d',
                            borderBottomWidth: 1,
                        }}
                    />




                    {/* TEXT INPUT*/}

                    <View style={styles.textInputSection}>
                        <Image source={require('./img/Ellipse 31 copy 8.png')} style={{ width: 60, height: 60, flex: 1, }} />

                        <TextInput
                            autoCorrect={false}
                            underlineColorAndroid="white"
                            placeholder="Add additional comments/questions "
                            placeholderTextColor="#e5eafa"
                            style={{ width: 130, fontSize: 12, fontFamily: 'Montserrat-Regular', color: 'white', flex: 4, marginRight: 10, }}></TextInput>

                    </View>


                    <TouchableOpacity style={{ marginTop: 30 }} onPress={this.props.onSave}>
                        <Text style={{ backgroundColor: 'transparent', color: 'white', fontSize: 20, fontFamily: 'Montserrat-Regular' }}>SAVE</Text>
                    </TouchableOpacity>


                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 'auto',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'rgba(50, 50, 50, 0.5)',
    },
    popupContainer: {
        marginTop: 50,
        width: 320,
        height: 450,
        backgroundColor: '#3c4650',
        borderColor: '#58a4b0',
        borderWidth: 2,
        alignItems: 'center',
    },
    header: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 10
    },
    addressLocation: {
        width: width,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',


    },
    icons: {
        width: 60,
        height: 60,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',


    },
    brush: {
        width: 25,
        height: 25,
        position: 'absolute'
    },
    blueCircle: {
        width: 90,
        height: 90,

    },
    brushContainer: {
        width: width - 30,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        justifyContent: 'space-around',
        margin: 20,


    },
    sortdown: {
        width: 18,
        height: 13,
    },
    dropdown: {
        width: width - 5,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 20,
        marginTop: 15
    },
    textInputSection: {
        width: width,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingHorizontal: 20,
    }
});
