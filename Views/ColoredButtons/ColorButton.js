import React, { Component } from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native'

export default class ColorButton extends Component{
  constructor(props){
    super(props)
  }

  render(){
    let buttonStyles = [styles.coloredButton, { backgroundColor: this.props.color }];
    if(this.props.selected){
      buttonStyles.push({
        borderColor: 'white', borderWidth: 2
      })
    }
    return (
      <TouchableOpacity style={buttonStyles} onPress={this.props.onPress}>
          <Text style={{ backgroundColor: 'transparent', fontSize: 10, fontFamily: 'Montserrat-Light', color: 'white', textAlign: 'center' }}>{this.props.text.toUpperCase()}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  coloredButton: {
      width: 80,
      height: 22,
      borderRadius: 15,
      justifyContent: 'center'
  },
})
