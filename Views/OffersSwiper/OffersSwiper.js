import React, {Component} from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  ScrollView
} from 'react-native';

import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default class OffersSwiper extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return (
      <View style={styles.container}>

        <View style={{ flexDirection: 'row', justifyContent: 'center', width: width - 40, marginBottom: 5 }}>
            <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Regular', textAlign: 'center', color: 'black' }}>PLACED OFFERS</Text>
        </View>




        <View style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: width,
            flexDirection: 'row',
            marginTop: 5,
        }} >
            <View style={{ width: (width - 20) / 4 + 20, }}>
                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>COMPANY</Text>
            </View>
            <View style={{ width: (width - 20) / 4 + 20, }}>
                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>RATING</Text>
            </View>
            <View style={{ width: (width - 20) / 4 - 20, }}>
                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>PRICE</Text>
            </View>
            <View style={{ width: (width - 20) / 4 - 20, }}>
                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'right', color: 'black', backgroundColor: 'transparent' }}>REACTION</Text>
            </View>
        </View>



        {/* LINE */}
        <View style={styles.line} />




        {/* FOUR SECTIONS FIRST */}
        <View style={styles.fourSections}>

            <View style={[styles.fourSectionsBigContainer, { flexDirection: 'row' }]} >
                <View style={{
                  flex: 3,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                  <View style={{
                    width: 30,
                    height: 30,
                    borderRadius: 15,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    shadowOffset:{ width: 0,  height: 0 },
                    shadowColor: '#e3605b',
                    shadowRadius: 4,
                    shadowOpacity: 0.8,
                  }}>
                    <Image source={require('./img/profileMan.png')} style={{ width: 30, height: 30, borderRadius: 15 }} />
                  </View>
                </View>
                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', color: 'black', backgroundColor: 'transparent', flexWrap: 'wrap', flex: 5 }}>Marble Arch  Repairs Ltd.</Text>
            </View>

            <View style={styles.fourSectionsBigContainer}>
                <View style={{ flexDirection: 'row' }}>
                    <View>
                        <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                    </View>
                    <View>
                        <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                    </View>
                    <View>
                        <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                    </View>
                    <View>
                        <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                    </View>
                    <View>
                        <Image source={require('./img/Shape 907 copy 4.png')} style={{ width: 15, height: 15, }} />
                    </View>
                </View>
                <View>
                    <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>(129)</Text>
                </View>
            </View>

            <View style={styles.fourSectionsSmallContainer}>
                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>EUR149</Text>
            </View>
            <View style={styles.fourSectionsSmallContainer}>
                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>20.10.17  @17:00h</Text>
            </View>
        </View>

        {/* TEXT CONTAINER */}

        <View style={{ width: width, justifyContent: 'flex-end', flexDirection: 'row', }}>

            <View style={{ backgroundColor: '#9d9db4', borderRadius: 10, width: width - 80, marginHorizontal: 10, justifyContent: 'flex-end' }}>
                <ScrollView>
                    <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white', marginLeft: 8 }}>Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, dolor Lorem ipsum dolor sit amet, adipiscing elit</Text>
                </ScrollView>
            </View>
        </View>

        <View style={{
            marginTop: 10,
            width: width - 40,
            flexDirection: 'row',
            justifyContent: 'center',
            justifyContent: 'space-between',
            alignContent: 'center',

        }}>

            <View >
                <TouchableOpacity style={{ width: ((width - 40) / 3) - 10, height: 35, borderRadius: 5, backgroundColor: '#006400', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderColor: 'black', borderWidth: 1 }}>
                    <Image source={require('./img/accept.png')} style={{ width: 30, height: 30, }} />
                    <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center', marginRight: 5 }}>ACCEPT</Text>
                </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity style={{ width: ((width - 40) / 3) - 10, height: 35, borderRadius: 5, backgroundColor: '#ff0000', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderColor: 'black', borderWidth: 1 }}>
                    <Image source={require('./img/decline.png')} style={{ width: 30, height: 30, }} />
                    <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center', marginRight: 5 }}>DECLINE</Text>
                </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity style={{ width: ((width - 40) / 3) - 10, height: 35, borderRadius: 5, backgroundColor: '#ffa500', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderColor: 'black', borderWidth: 1 }}>
                    <Image source={require('./img/snooze-icon.png')} style={{ width: 30, height: 30, }} />
                    <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center', marginRight: 5 }}>SNOOZE</Text>
                </TouchableOpacity>
            </View>
        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    headerConainer: {
        width: width,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: 470
    },
    locationContainer: {
        width: 200,
        height: 35,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(44,57,66,0.5)',
        left: -60
    },
    location: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    fourSectionsSmallContainer: {
        width: (width - 20) / 4 - 20,
        height: 50,
        flexDirection: 'row',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fourSectionsBigContainer: {
        width: (width - 20) / 4 + 20,
        height: 50,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fourSections: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: 50,
        flexDirection: 'row',
        marginTop: 5,
    },
    line: {
        width: width - 40,
        borderBottomColor: '#999ea3',
        borderBottomWidth: 1,
    },
});
