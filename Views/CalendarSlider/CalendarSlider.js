import React, { Component } from 'react';

import {
  View,
  FlatList,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

var moment = require('moment');

export default class CalendarSlider extends Component{
  constructor(props){
    super(props);
    this.state = {
      lastDate: moment(new Date()),
      dates: [],
    }
  }

  componentWillMount(){
    this.parseDates();
    console.log("Will Mount");
  }

  parseDates(){
    let newDates = [];
    let oldDates = this.state.dates.slice(0);
    for(i = 0; i<5; i++){
      let d = this.state.lastDate.add(1, 'M').clone();
      newDates.push({
        key: d.toDate().getTime(),
        date: d,
        selected: i == 0,
      });
    }
    newDates = oldDates.concat(newDates);
    this.setState({dates: newDates});
  }

  selectDate(key){
    let dates = this.state.dates.slice(0);
    dates.forEach((date) => {
      if(key == date.key){
        date.selected = true;
      }else{
        date.selected = false;
      }
    });
    this.setState({dates: dates});
  }

  render(){
    return (
      <View style={styles.monthCont}>
        <FlatList
          data={this.state.dates}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={({item}) => {

            let viewStyles = [
              { width: 70, flexDirection: 'row', paddingHorizontal: 2 }
            ]
            let textColor = [];
            if(item.selected){
              viewStyles.push({
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderBottomColor: 'white',
              });
              textColor.push({color: 'white'});
            }
            return (
              <TouchableOpacity onPress={() => {
                this.selectDate(item.key);
              }}>
                  <View style={viewStyles}>
                      <Text style={[styles.monthTextStyle, textColor]}>{item.date.format('MMM').toUpperCase()}</Text>
                      <Text style={[{ color: '#989ea3', backgroundColor: 'transparent' }, textColor]}>{item.date.format('YYYY')}</Text>
                  </View>
              </TouchableOpacity>
            )
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  monthTextStyle: {
      color: '#989ea3',
      marginRight: 2,
      textAlign: 'center',
      fontFamily: 'Montserrat-Light',
      backgroundColor: 'transparent'
  },
  monthCont: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignContent: 'center',
  },
})
