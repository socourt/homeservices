import React, { Component } from 'react';
import Dimensions from 'Dimensions';

import {
    View,
    FlatList,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

var moment = require('moment');
var { height, width } = Dimensions.get('window');

export default class ServiceType extends Component {
    constructor(props) {
        super(props)
        this.state = {
            'cleaning-black': require('./img/cleaning-black.png'),
            'cleaning-silver': require('./img/cleaning-silver.png'),
            'cleaning-white': require('./img/cleaning-white.png'),
            'cleaning-orange': require('./img/cleaning-orange.png'),
            'electrician-black': require('./img/electrician-black.png'),
            'electrician-silver': require('./img/electrician-silver.png'),
            'electrician-white': require('./img/electrician-white.png'),
            'electrician-orange': require('./img/electrician-orange.png'),
            'painter-roller-black': require('./img/painter-roller-black.png'),
            'painter-roller-silver': require('./img/painter-roller-silver.png'),
            'painter-roller-white': require('./img/painter-roller-white.png'),
            'painter-roller-orange': require('./img/painter-roller-orange.png'),
            'question-mark-black': require('./img/question-mark-black.png'),
            'question-mark-silver': require('./img/question-mark-silver.png'),
            'question-mark-white': require('./img/question-mark-white.png'),
            'question-mark-orange': require('./img/question-mark-orange.png'),
        }
    }

    render() {

        let image = './img/' + this.props.image + '-' + this.props.color;
        let iconStyles = [styles.cleaningIcon];
        let textColor = [styles.textColor];

        if (this.props.bold) {
            textColor.push({ fontWeight: 'bold' });
        }

        switch (this.props.color) {
            case 'orange':
                iconStyles.push({
                    backgroundColor: '#0c1b54',
                    borderColor: '#dfaa00',
                });
                textColor.push({ color: "#02124d" })
                break;
            case 'silver':
                iconStyles.push({
                    backgroundColor: 'transparent',
                    borderColor: '#7f858c',
                });
                textColor.push({ color: "#7f858c" })
                break;
            case 'white':
                iconStyles.push({
                    borderColor: 'white',
                    backgroundColor: '#0c1b54',
                });
                textColor.push({ color: "white" })
                break;
            case 'black':
                iconStyles.push({
                    borderColor: 'black',
                    backgroundColor: 'white',
                });
                textColor.push({ color: "black" })
                break;
        }
        return (
            <View style={styles.serciceSmallContainer}>
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }} onPress={this.props.onPress}>
                    <View style={iconStyles}>
                        <Image resizeMode="contain" source={this.state[`${this.props.image}-${this.props.color}`]} style={styles.brush} />
                    </View>
                    <Text style={textColor}>{this.props.textTypeService.toUpperCase()}</Text>
                </TouchableOpacity>

                {this.props.option && (
                    <View style={styles.smallPencil}>
                        <TouchableOpacity onPress={this.props.onPress} onPress={this.props.pressOption}>
                            <Image source={require('./img/pencil-edit-button.png')} style={{ width: 16, height: 16 }} />
                        </TouchableOpacity>
                    </View>
                )}

            </View >

        )
    }
}

const styles = StyleSheet.create({
    smallPencil: {
        width: 20,
        height: 20,
        position: 'absolute',
        backgroundColor: '#76c3f7',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        top: 5,
        right: 0
    },
    textColor: {
        fontSize: 9,
        fontFamily: 'Montserrat-Light',
        textAlign: 'center',
        backgroundColor: 'transparent',
    },
    cleaningIcon: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        marginBottom: 4,
    },
    serciceSmallContainer: {
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start',
        height: 80,
        width: (width - 60) / 4,
    },
    brush: {
        width: 21,
    },
});
