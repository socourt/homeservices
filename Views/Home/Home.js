import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import ServiceType from '../ServiceType/ServiceType';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    StatusBar,
    TextInput,
    ScrollView,
    KeyboardAvoidingView,
    FlatList,
    Modal
} from 'react-native';
import Dimensions from 'Dimensions';
import PopUp from '../PopUp/PopUp';

import Swiper from 'react-native-swiper';
import Lightbox from 'react-native-lightbox';

var { height, width } = Dimensions.get('window');

export default class Home extends Component {
    static navigationOptions = {
        title: 'Home',
    }
    constructor(props) {
        super(props)
        this.state = {
            selectedCategory: 'cleaning',
            displayModal: false,
            categories: [
                {
                    image: 'cleaning',
                    service: 'cleaning',
                },
                {
                    image: 'electrician',
                    service: 'electrician services',
                },
                {
                    image: 'painter-roller',
                    service: 'wall paint',
                },
                {
                    image: 'question-mark',
                    service: 'others (specify)',
                },
            ]
        }
    }
    render() {

        return (
            <View style={styles.container}>

                <StatusBar
                    backgroundColor="#3fabbe"
                    barStyle="light-content"
                />

                {/* HEADER */}
                <Header
                    icon="none"
                    navigationType="title"
                    title="SUBMIT NEW HOME SERVICE ORDER"
                    close="close"
                    titleStyles={{ color: 'black', textDecorationLine: 'underline' }}
                    onClose={() => {
                        this.props.navigation.goBack();
                    }}
                />



                {/* MAIN CONTAINER */}
                <ScrollView contentContainerStyles={{
                    height: height - 110,
                    width: width - 10,
                    backgroundColor: 'white',
                }}>
                    <View style={{
                        width: width - 10,
                        height: height - 110,
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'white',
                        marginVertical: 12,
                        borderRadius: 5,

                    }}>
                        {/* ADD PHOTOS */}
                        <View style={styles.addPhotos}>
                            <Text style={styles.topText} >ADD PHOTOS</Text>
                            <Text style={styles.leftText}>1</Text>
                            <View style={styles.topImgCont}>

                                <Swiper
                                    width={width - 70}
                                    height={150}
                                    style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
                                    showsButtons={false}
                                    paginationStyle={{
                                        bottom: 10
                                    }}
                                    dot={<View style={{ backgroundColor: 'white', borderWidth: StyleSheet.hairlineWidth, borderColor: 'black', width: 6, height: 6, borderRadius: 3, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
                                    activeDot={<View style={{ backgroundColor: '#004ca0', borderWidth: StyleSheet.hairlineWidth, borderColor: 'black', width: 6, height: 6, borderRadius: 3, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
                                >
                                    <Lightbox
                                        style={styles.lightbox}>
                                        <Image
                                            style={styles.topImage}
                                            source={require('./img/topImage.png')}
                                        />
                                    </Lightbox>
                                    <Lightbox
                                        style={styles.lightbox}>
                                        <Image
                                            style={styles.topImage}
                                            source={require('./img/topImage.png')}
                                        />
                                    </Lightbox>
                                    <Lightbox
                                        style={styles.lightbox}>
                                        <Image
                                            style={styles.topImage}
                                            source={require('./img/topImage.png')}
                                        />
                                    </Lightbox>

                                </Swiper>


                                <TouchableOpacity style={styles.camButt}>
                                    <Image source={require('./img/add-photos-bttn.png')} style={{ width: 30, height: 25 }} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.remove}>
                                    <Image source={require('./img/remove.png')} style={{ width: 20, height: 20 }} />
                                </TouchableOpacity>
                            </View>
                        </View>

                        {/* ASSIGN TO */}
                        <View style={styles.assign}>
                        </View>

                        {/* ICON CONTAINER */}
                        <View style={styles.iconsSectionContainer}>
                            <Text style={styles.topText} > A CATEGORY + PREFERENCES</Text>
                            <Text style={styles.leftText}>2</Text>
                            <FlatList
                                horizontal={true}
                                data={this.state.categories}
                                showsHorizontalScrollIndicator={false}
                                renderItem={({ item }) => {
                                    if (item.image == this.state.selectedCategory) {
                                        console.log(item);
                                        return (
                                            <ServiceType
                                                color="orange"
                                                onPress={() => {
                                                    this.setState({ selectedCategory: item.image, categories: this.state.categories.slice(0) });
                                                }}
                                                option={item.image == 'cleaning'}
                                                image={item.image}
                                                pressOption={() => {
                                                    this.setState({ displayModal: true })
                                                }}
                                                textTypeService={item.service}
                                                bold={true}
                                                icon="none"
                                            />
                                        );
                                    }
                                    return (
                                        <ServiceType color="black" onPress={() => {
                                            this.setState({ selectedCategory: item.image, categories: this.state.categories.slice(0) });
                                        }} image={item.image} textTypeService={item.service} />
                                    );

                                }}
                            />

                        </View>


                        {/*LOCATION  */}

                        <View style={styles.mapSectionContainer}>
                            <Text style={styles.topText} >LOCATION</Text>
                            <Text style={styles.leftText}>3</Text>
                            <View style={{
                                marginHorizontal: 20,
                                marginVertical: 5,
                                display: 'flex',
                                shadowOffset: { width: 1, height: 1, },
                                shadowColor: 'black',
                                shadowOpacity: 0.5,
                            }}
                            >
                                <Image source={require('./img/map.png')} style={{ width: width - 60, height: 60, borderRadius: 5 }} />
                            </View>
                            <Text style={{ backgroundColor: 'transparent', fontSize: 14, fontFamily: 'Montserrat-Light', left: 25, top: 60, position: 'absolute', color: 'black' }}>9WA 0WS, London, UK</Text>
                            <Image source={require('./img/map_pin.png')} style={{ width: 50, height: 50, right: 100, top: 20, position: 'absolute' }} />
                        </View>
                        <View style={styles.paymentSectionContainer}>
                            <Text style={styles.topText} >PAYMENT</Text>
                            <Text style={styles.leftText}>4</Text>
                            <View style={styles.paymentContainer}>
                                <Image source={require('./img/card.png')} style={{ width: 30, height: 25, marginHorizontal: 15 }} />
                                <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    placeholder="**** **** ****2828 "
                                    placeholderTextColor="black"
                                    style={{ width: 100, textAlign: 'center', color: 'white', fontSize: 10 }}></TextInput>
                                <TouchableOpacity>
                                    <Image source={require('./img/pencil-edit-button.png')} style={{ width: 25, height: 25, marginHorizontal: 2 }} />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Image source={require('./img/plus.png')} style={{ width: 25, height: 25, marginHorizontal: 2 }} />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Image source={require('./img/checkedblack.png')} style={{ width: 20, height: 20, marginHorizontal: 2 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>



                {/* FOOTER */}
                <Footer icon={['user', 'home']} navigationType="text" title="order"
                    homePress={() => {
                        this.props.navigation.navigate('FeedUpcoming');
                    }}
                    userPress={() => {
                        this.props.navigation.navigate('MyProfile');
                    }}
                    onPressText={() => {
                        this.props.navigation.navigate('BusinessOrderReview');
                    }}
                />

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.displayModal}
                    onRequestClose={() => { alert("Modal has been closed.") }}
                >
                    <PopUp onClose={() => { this.setState({ displayModal: false }) }} onSave={() => { this.setState({ displayModal: false }) }} />
                </Modal>

            </View>


        );
    }
}

const styles = StyleSheet.create({
    lightbox:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    open: {
        width: width - 10,
        height: 600
    },
    paymentContainer: {
        width: width - 70,
        marginHorizontal: 20,
        marginVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 5,
        shadowOffset: { width: 1, height: 1, },
        shadowColor: 'black',
        shadowOpacity: 0.5,
    },
    leftText: {
        fontSize: 14,
        fontFamily: 'Montserrat-Light',
        color: 'black',
        position: 'absolute',
        top: 20,
        left: -10,
        backgroundColor: 'white',
        paddingHorizontal: 5,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: 'black',
        zIndex: 10,
    },
    topText: {
        fontSize: 14,
        fontFamily: 'Montserrat-Light',
        color: 'black',
        position: 'absolute',
        top: -10,
        backgroundColor: 'white',
        paddingHorizontal: 5,
    },
    iconsSectionContainer: {
        width: width - 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'black',
        paddingVertical: 20,
        top: -1,
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    header: {
        width: width,
        height: 50,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        flexDirection: 'row',
    },

    addPhotos: {
        width: width - 40,
        height: 180,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'black',
        borderWidth: 1,
    },
    topImgCont: {
        width: width,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
    },
    topImage: {
        flex: 1,
        width: width,
        height: height,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    camButt: {
        position: 'absolute',
        width: 40,
        height: 30,
        top: 110,
        right: 30
    },
    remove: {
        position: 'absolute',
        width: 20,
        height: 20,
        marginLeft: 10,
        top: 10,
        right: 40
    },

    icons: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderColor: '#dfaa00',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        backgroundColor: '#0c1b54'
    },
    iconsNotBold: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderColor: 'black',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
    },
    brush: {
        width: 25,
        height: 25,
    },
    iconsImg: {
        width: 30,
        height: 35,
    },
    mapSectionContainer: {
        width: width - 40,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        top: -2,
    },
    paymentSectionContainer: {
        width: width - 40,
        height: 50,
        justifyContent: 'center',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        marginBottom: 10,
        top: - 3,
    },

});



