import React, {Component} from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  TextInput
} from 'react-native';

import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window')

export default class PopUp extends Component{
  constructor(props){
    super(props);
  }

  render(){
    return (
      <View style={styles.container}>

          <View style={styles.popupContainer}>

              <Image source={require('./img/Layer 2.png')} style={styles.background} />
              {/* HEADER */}
              <View style={styles.header}>
                  <Text style={{ backgroundColor: 'transparent',fontSize: 18, fontFamily: 'Montserrat-Regular', color: 'white', textAlign: 'center' }}>ADD PREFERENCES</Text>
                  <TouchableOpacity style={{ alignItems: 'center', position: 'absolute', right: -55, height: 20 }}
                    onPress={this.props.onClose}
                  >
                      <Image source={require('./img/remove copy.png')} style={{ width: 20, height: 20, }} />
                  </TouchableOpacity>
              </View>


              {/* ADDRESS LOCATION */}
              <View style={styles.addressLocation}>
                  <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                      <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                          <Text style={{ backgroundColor: 'transparent',fontSize: 14, fontFamily: 'Montserrat-Regular', color: '#627485' }}>Location for the home service order:</Text>
                          <Text style={{ backgroundColor: 'transparent',fontSize: 14, fontFamily: 'Montserrat-Regular', color: '#58a4b0' }}>Ludwigstrasse 41, 80012, Munich</Text>
                      </View>
                      <TouchableOpacity style={{ alignItems: 'center', position: 'absolute', right: -25, height: 30 }} >
                          <Image source={require('./img/pen blue.png')} style={{ width: 30, height: 30, }} />
                      </TouchableOpacity>
                  </View>
              </View>


              {/* IMAGES*/}
              <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column',marginTop:20 }}>
                  <Image source={require('./img/dash.png')} style={{ position:'absolute',top:-50}} />
                  <Image source={require('./img/Shape171.png')} style={{ width: 25, height: 25, }} />
              </View>

              <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                  <Text style={{ backgroundColor: 'transparent',fontSize: 13, fontFamily: 'Montserrat-Regular', color: 'white', borderBottomColor: '#353e48', borderBottomWidth: 1, paddingBottom: 10, }}>keep the info as template?</Text>
                  <Text style={{ backgroundColor: 'transparent',fontSize: 13, fontFamily: 'Montserrat-Regular', color: '#e5eafa', }}>Amount of sq meters in the property:</Text>
              </View>



              {/* BRUSH CONTAINER*/}
              <View style={styles.brushContainer}>
                  <View style={{ width: 45, height: 30, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ backgroundColor: 'transparent',color: 'white' }}> 0 </Text>
                  </View>
                  <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                      <View style={styles.icons}>
                          <Image source={require('./img/blue circle.png')} style={styles.blueCircle} />
                          <Image source={require('./img/cleaning.png')} style={styles.brush} />
                      </View>
                      <Text style={{ backgroundColor: 'transparent',textDecorationLine: 'underline', width: 100, fontSize: 14, fontFamily: 'Montserrat-Regular', color: 'white', borderBottomColor: '#353e48', textAlign: 'center', top: -10 }}>70sqm</Text>
                  </View>
                  <View style={{ width: 45, height: 30, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center' }}>
                      <Text style={{ backgroundColor: 'transparent',color: 'white' }}> 150 </Text>
                  </View>
              </View>


              {/* DROPDOWN*/}
              <View style={styles.dropdown}>

                  <TouchableOpacity>
                      <View style={{ width: 33, height: 24, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center' }}>
                          <Image source={require('./img/sort-down.png')} style={styles.sortdown} />
                      </View>
                  </TouchableOpacity>
                  <Text style={{ backgroundColor: 'transparent',color: 'white', fontFamily: 'Montserrat-Regular', fontSize: 12 }}> bedrooms & </Text>

                  <TouchableOpacity>
                      <View style={{ width: 33, height: 24, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center' }}>
                          <Image source={require('./img/sort-down.png')} style={styles.sortdown} />
                      </View>
                  </TouchableOpacity>
                  <Text style={{ backgroundColor: 'transparent',color: 'white', fontFamily: 'Montserrat-Regular', fontSize: 12 }}> bathrooms in the property </Text>

              </View>


              {/* TEXT INPUT*/}
              <View style={styles.textInputSection}>
                  <Image source={require('./img/Ellipse 31 copy 8.png')} style={{ width: 60, height: 60}} />
                  <View style={{ width: width - 140, height: 20, display: 'flex', alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1, borderBottomColor: 'white' }}>
                    <TextInput
                      style={{height: 20, color: 'white', fontSize: 12}}
                      placeholder="Other Wishes"
                      placeholderTextColor="white"
                    />
                  </View>
              </View>
              <TouchableOpacity style={{marginTop:15}} onPress={this.props.onSave}>
                  <Text style={{ backgroundColor: 'transparent',color: 'white', fontSize: 20, fontFamily: 'Montserrat-Regular' }}>SAVE</Text>
              </TouchableOpacity>


          </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    background: {
      width: 320,
      height: 450,
      position: 'absolute',
      top: 0,
      left: 0,
      opacity: 0.6
    },
    container: {
        flex: 1,
        height: 'auto',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'rgba(50, 50, 50, 0.6)',
    },
    popupContainer: {
        marginTop: 50,
        width: 320,
        height: 450,
        backgroundColor: '#3c4650',
        borderColor: '#58a4b0',
        borderWidth: 2,
        alignItems: 'center',
    },
    header: {
        marginTop:10,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    addressLocation: {
        width: width,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',


    },
    icons: {
        width: 60,
        height: 60,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',


    },
    brush: {
        width: 25,
        height: 25,
        position: 'absolute'
    },
    blueCircle: {
        width: 90,
        height: 90,

    },
    brushContainer: {
        width: width - 30,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
    },
    sortdown: {
        width: 16,
        height: 11,
        position: 'absolute'
    },
    dropdown: {
        width: width - 5,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 20,
        marginTop:15
    },
    textInputSection: {
        width: width,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }
});
