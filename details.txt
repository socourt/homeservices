1. Header - multi functional single component header fitting all screens in the app                                     |    - 2h
--------------------------------------------------------------------------------------------------------------------------------------------
2. Navigation bar footer - single component fitting all screens in the app with specific option for highlight an icon   |    - 2h
--------------------------------------------------------------------------------------------------------------------------------------------
3. Home                                                                                                                 |    - 8h
  - Upcomming -                                                                                                  - 2h   |
      - Slider with cards displaying information for upcoming services                                                  |
      - Header with - menu / upcoming / featured buttons                                                                |
      - Standard footer navigation bar with highlight on home icon                                                      |
  - Featured -                                                                                                   - 2h   |
      - Slider with cards displaying featured companies                                                                 |
      - Header with - menu / upcoming / featured / more                                                                 |
      - Standard footer navigation bar with highlight on home icon                                                      |
  - Upcoming Details -                                                                                           - 4h   |
    - Header with - back / upcoming / featured                                                                          |
    - Standard footer navigation bar with highlight on home icon                                                        |
    - User avatar of the sender, distance to the problem                                                                |
    - Details of the sender of the request                                                                              |
    - Photos of the house / problem                                                                                     |
    - Order execution details                                                                                           |
    - Chat with company button                                                                                          |
    - Optional request payment button for business user                                                                 |
    - Respond to payment - optional for normal user                                                                     |
--------------------------------------------------------------------------------------------------------------------------------------------
4. Single notification component                                                                                        |    - 1h
  - Displaying time                                                                                                     |
  - Display Message                                                                                                     |
  - Clear button on swipe left                                                                                          |
  - Functional button with optional icon / text on swipe left                                                           |
--------------------------------------------------------------------------------------------------------------------------------------------
5. Notification                                                                                                         |    - 3h
  - Header with - notifications icon / upcoming / featured                                                              |
  - Standard footer navigation bar                                                                                      |
  - List of single notification components                                                                              |
--------------------------------------------------------------------------------------------------------------------------------------------
6. Orders slider (horizontal)                                                                                           |    - 2h
  - Display date                                                                                                        |
  - Category                                                                                                            |
  - Avatar - user / company                                                                                             |
  - Name - user/ company                                                                                                |
  - Click to pay (optional)                                                                                             |
  - Address (optional)                                                                                                  |
--------------------------------------------------------------------------------------------------------------------------------------------
7. Me screen - normal user                                                                                              |    - 4h
  - Header with menu / title                                                                                            |
  - Standard footer navigation bar with highlight on user icon                                                          |
  - User avatar with photo and name                                                                                     |
  - User details with optional edit                                                                                     |
  - Payment details - add or change card                                                                                |
  - Months slider (horizontal)                                                                                          |
  - Orders slider component with specific parameters                                                                    |
  - Share app button                                                                                                    |
  - Language and About buttons                                                                                          |
--------------------------------------------------------------------------------------------------------------------------------------------
8. Calendar component slider                                                                                            |    - 4h
  - Moder for days / months                                                                                             |
  - Formatting the display of the date                                                                                  |
  - Option for selecting a single date-time slot - (function returning the selected date on change)                     |
  - Inital selected date                                                                                                |
--------------------------------------------------------------------------------------------------------------------------------------------
9. Provided services component                                                                                          |    - 1h
  - Title                                                                                                               |
  - Horizontal slider with categories displayed with name and icon (optional selection one or more categories)          |
--------------------------------------------------------------------------------------------------------------------------------------------
10. Me screen - business user                                                                                           |    - 4h
  - Header with menu / title                                                                                            |
  - Standard footer navigation bar with highlight on user icon                                                          |
  - Company avatar with company name (option to change image)                                                           |
  - Company details with optional edit                                                                                  |
  - Bank account details                                                                                                |
  - Provided services component                                                                                         |
  - Workers with icon and number of workers                                                                             |
  - Company rating                                                                                                      |
  - Legal details                                                                                                       |
  - Upcoming orders                                                                                                     |
    - Filter                                                                                                            |
    - Calendar component slider                                                                                         |
    - Orders slider                                                                                                     |
--------------------------------------------------------------------------------------------------------------------------------------------
11. Chat screen                                                                                                         |    - 2h
  - Header with back / title                                                                                            |
  - Text input for the message                                                                                          |
  - Displayed list of messages                                                                                          |
--------------------------------------------------------------------------------------------------------------------------------------------
12. Create New Order                                                                                                    |    - 4h
  - Header with title / close                                                                                           |
  - Standard footer navigation bar with order button in the middle                                                      |
  - Photos section                                                                                                      |
    - Slider with added images                                                                                          |
  - Select category section                                                                                             |
    - Additional modal for displaying details of the selected section if needed                                         |
      - Title                                                                                                           |
      - Address                                                                                                         |
      - Slider with sqm                                                                                                 |
      - bedrooms/bathrooms                                                                                              |
      - wishes                                                                                                          |
  - Location section - Based on current location                                                                        |
  - Displaying / changing the payment details of the current user                                                       |
--------------------------------------------------------------------------------------------------------------------------------------------
13. Receive Request Business user:                                                                                      |    - 5h
  - Header with - back icon / upcoming / featured                                                                       |
  - Standard footer navigation bar with bell button in the middle highlighted home icon                                 |
  - Requestor details                                                                                                   |
  - Photos                                                                                                              |
  - Accept / Decline / Snooze section                                                                                   |
  - Modal for accept request                                                                                            |
    - Title                                                                                                             |
    - Address                                                                                                           |
    - Price offer                                                                                                       |
    - Date & time for execution of the service                                                                          |
    - Comments/Questions section                                                                                        |
    - Save button                                                                                                       |
  - Display offers section ( custom component )                                                                         |
    - List of specific offers displaying: Company Avatar / Company rating / Price offered / Date-time of the execution  |
--------------------------------------------------------------------------------------------------------------------------------------------
14. Select business offer - normal user                                                                                 |    - 3h
  - Requestor details                                                                                                   |
  - Pictures of the problem                                                                                             |
  - Slider with specific offers for the requested service                                                               |
  - Header with - back icon / upcoming / featured                                                                       |
  - Standard footer navigation bar with bell button in the middle highlighted home icon                                 |
--------------------------------------------------------------------------------------------------------------------------------------------
TOTAL                                                                                                                   |    - 45h
--------------------------------------------------------------------------------------------------------------------------------------------
