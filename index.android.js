import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View } from 'react-native';
import Home from './Views/Home/Home';
import popupsmall from './Views/popupsmall/popupsmall';
import FeedUpcoming from './Views/FeedUpcoming/FeedUpcoming';
import FeedFeatured from './Views/FeedFeatured/FeedFeatured';
import MyProfile from './Views/MyProfile/MyProfile';
import MyBusinessProfile from './Views/MyBusinessProfile/MyBusinessProfile';
import BusinessOrderReview from './Views/BusinessOrderReview/BusinessOrderReview';
import OfferReview from './Views/OfferReview/OfferReview';
import OrderSummary from './Views/OrderSummary/OrderSummary';
import Summary from './Views/Summary/Summary';
import Chat from './Views/Chat/Chat';
import BusinessAcceptDetails from './Views/BusinessAcceptDetails/BusinessAcceptDetails';
import NotificationScr from './Views/NotificationScr/NotificationScr';
import InitialScreen from './Views/InitialScreen/InitialScreen';
import SingInFormPopup from './Views/SingInFormPopup/SingInFormPopup';
import { StackNavigator } from 'react-navigation';

const homeservices = StackNavigator({
  InitialScreen: { //
    screen: InitialScreen
  },
  FeedUpcoming: { //
    screen: FeedUpcoming
  },
  SingInFormPopup: { //
    screen: SingInFormPopup
  },
  Home: {
    screen: Home
  },
  MyBusinessProfile: { //
    screen: MyBusinessProfile
  },
  FeedFeatured: { //
    screen: FeedFeatured
  },
  MyProfile: { //
    screen: MyProfile
  },
  popupsmall: {
    screen: popupsmall
  },
  BusinessOrderReview: {
    screen: BusinessOrderReview
  },
  OfferReview: {
    screen: OfferReview
  },
  OrderSummary: {
    screen: OrderSummary
  },
  Summary: {
    screen: Summary
  },
  Chat: {
    screen: Chat
  },
  BusinessAcceptDetails: {
    screen: BusinessAcceptDetails
  },
  NotificationScr: {
    screen: NotificationScr
  },
},

  {
    headerMode: 'none'
  }
)

AppRegistry.registerComponent('homeservices', () => homeservices)
// react-native bundle --entry-file index.ios.js --platform ios --dev false --bundle-output ios/main.jsbundle --assets-dest ios
